'use strict';

var todo = new function() {
    // this.items = [window.localStorage.getItem("item")];
    if (localStorage.getItem("items") != null) {

        this.items = JSON.parse(localStorage.getItem("items"));
    }
    else {
        this.items = [];

    }

    // Prints out the todo.items and localStorage to see if they match, simple devTest
    this.testStorage = function () {

        console.log("todo.items: " + JSON.stringify(todo.items));
        console.log("localStorage: " + window.localStorage.getItem("items"));
    }

    // Populate app with items.
    this.getItems = function () {
        // If there are any saved items, populate the app
        if (todo.items.length > 0) {
            console.log("Rendering new items");

            var myList = document.getElementById("myList"); // Gets the UL element and its children

            // Removes all nodes (otherwise there will be duplicates)
            while (myList.firstChild) {
                myList.removeChild(myList.firstChild);

            }
            // Repopuplate with new elements
            for (let i = 0; i<todo.items.length; i++) {

                let item = document.createElement('li'); // Creates a list element
                item.classList.add("item"); // Adds the selector class "item" to the list element
                let itemContent = document.createTextNode(todo.items[i]); // Each items stringvalue
                //console.log("ItemContent from load: " + itemContent);
                item.appendChild(itemContent); // Adds the text content to the element

                // Eventlistener that checks for a click on an element, fires the removeItem function and removes the node
                //
                item.addEventListener("click", function(){
                    this.parentNode.removeChild(this); // Removes the clicked node from the DOM
                    todo.removeItem(i);

                }, false);



                myList.appendChild(item); // Adds the newly created list element as a child to the UL element to the DOM

            }

        }
    }




    this.addItem = function () {
        var myInput = document.getElementById("input"); // Input textfield

        // If no text has been added to the textfield, do not add any todo's
        if (myInput.value.length == 0){

            console.log("Empty textfield");
        }
        else {
            console.log("Adding item");
            todo.items.push(myInput.value);
            window.localStorage.setItem("items", JSON.stringify(todo.items));



            // Reloads the page that will trigger a new render of items
            location.reload();
            //this.getItems();

        }
    }

    this.removeItem = function (indexId) {

        console.log("Removes " + todo.items[indexId] + " from todo.items");
        todo.items.splice(indexId, 1);


        window.localStorage.setItem("items", JSON.stringify(todo.items));

        this.getItems();

    }


}

todo.getItems();
